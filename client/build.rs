
extern crate bindgen;

use std::env;

fn main() {
    let project_dir = env::var("CARGO_MANIFEST_DIR").unwrap();

    println!("cargo:rustc-link-search=native={}/../capi", project_dir);
    println!(r"cargo:rustc-link-lib=static=altv-capi-client");

    let bindings = bindgen::Builder::default()
        .header("../capi/client/altv-capi.hpp")
        .clang_arg("-DALT_CLIENT_API")
        .generate()
        .expect("Unable to generate client bindings");

    bindings
        .write_to_file(project_dir+"/src/altv_capi.rs")
        .expect("Couldn't write client bindings!");
}

