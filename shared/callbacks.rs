/**
 * 
 */

use altv_capi::*;

pub type OnMakeClientFn = extern "C" fn (
    * mut alt_IResource,
    * mut alt_IResource_CreationInfo,
    * mut alt_Array_String
);
pub type OnInstantiateFn = extern "C" fn (* mut alt_IResource) -> bool;
pub type OnStartFn = extern "C" fn (* mut alt_IResource) -> bool;
pub type OnStopFn = extern "C" fn (* mut alt_IResource) -> bool;
pub type OnEventFn = extern "C" fn (
    * mut alt_IResource,
    * mut alt_CEvent
) -> bool;
pub type OnTick = extern "C" fn (* mut alt_IResource);
pub type OnCreateBaseObjectFn = extern "C" fn (
    * mut alt_IResource,
    * mut alt_IBaseObject
);
pub type OnRemoveBaseObjectFn = extern "C" fn (
    * mut alt_IResource,
    * mut alt_IBaseObject
);
