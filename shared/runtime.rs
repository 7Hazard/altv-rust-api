/**
 * 
 */

// use resource::{
//     Resource,
//     CreationInfo
// };

use altv_capi::{
    alt_IScriptRuntime,
    alt_CAPIScriptRuntime_Create,
    alt_IResource_CreationInfo,
    alt_IResource
};

// type CCreateResourceFn = extern "C" fn(
//     * mut alt_IScriptRuntime,
//     *mut alt_IResource_CreationInfo
// ) -> *mut alt_IResource;

// type CRemoveResourceFn = extern "C" fn(
//     * mut alt_IScriptRuntime,
//     * mut alt_IResource
// );

// type COnTickFn = extern "C" fn(* mut alt_IScriptRuntime);

type CreateResourceFn = extern "C" fn(
    * mut alt_IScriptRuntime,
    *mut alt_IResource_CreationInfo
) -> *mut alt_IResource;

type RemoveResourceFn = extern "C" fn(
    * mut alt_IScriptRuntime,
    * mut alt_IResource
);

type OnTickFn = extern "C" fn(* mut alt_IScriptRuntime);

pub fn new(
    create_resource : CreateResourceFn,
    remove_resource : RemoveResourceFn,
    on_tick : OnTickFn
) -> *mut alt_IScriptRuntime
{
    unsafe {
        return alt_CAPIScriptRuntime_Create(
            Some(create_resource),
            Some(remove_resource),
            Some(on_tick)
        );
    }
}

// type CreateResourceFn = extern "C" fn(
//     * mut alt_IScriptRuntime,
//     *mut alt_IResource_CreationInfo
// ) -> *mut alt_IResource;

// type RemoveResourceFn = extern "C" fn(
//     * mut alt_IScriptRuntime,
//     * mut alt_IResource
// );

// type OnTickFn = extern "C" fn(* mut alt_IScriptRuntime);

// impl Runtime {
//     fn new(
//         createResourceFn : CreateResourceFn,
//         removeResourceFn : RemoveResourceFn,
//         onTickFn : OnTickFn
//     ) -> Runtime
//     {
//         return alt_CAPI_IScriptRuntime_Create(
//             Some(createResourceFn),
//             Some(removeResourceFn),
//             Some(onTickFn)
//         );
//     }
// }
