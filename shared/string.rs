/**
 * 
 */

use std::{
    ffi::{
        CString
    }
};

use altv_capi::{
    alt_String
};

use std::ffi::CStr;

pub fn from(astr : *mut alt_String) -> String
{
    return unsafe {
        CStr::from_ptr((*astr).data)
            .to_str().unwrap().to_owned()
    };
}

pub fn new(string: &str) -> alt_String
{
    let cstr = CString::new(string).expect("CString::new failed");

    let astr = alt_String {
        data: cstr.into_raw(),
        size: string.len() as u64
    };

    return astr;
}
